# Assignment - Java RPG Characters

A Java console application that implements RPG characters and items.

## Author

Fredrik Jeppsson

## Installation

This is a Maven project. Clone the repository and run unit tests by executing mvn test.

```bash
mvn test  # executes the project's tests.
```