package rpg;

import org.junit.jupiter.api.Test;
import rpg.heroes.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests that attributes are correct at level 1.
 */
public class TestDefaultAttributes {
    @Test
    void totalAttributes_levelOneMage_baseAttrs() {
        var hero = new Mage("test1");
        var expectedAttrs = new PrimaryAttributes(1, 1, 8);
        var actualAttrs = hero.totalAttributes();
        assertEquals(expectedAttrs, actualAttrs);
    }

    @Test
    void totalAttributes_levelOneRanger_baseAttrs() {
        var hero = new Ranger("test1");
        var expectedAttrs = new PrimaryAttributes(1, 7, 1);
        var actualAttrs = hero.totalAttributes();
        assertEquals(expectedAttrs, actualAttrs);
    }

    @Test
    void totalAttributes_leveOneRogue_baseAttrs() {
        var hero = new Rogue("test1");
        var expectedAttrs = new PrimaryAttributes(2, 6, 1);
        var actualAttrs = hero.totalAttributes();
        assertEquals(expectedAttrs, actualAttrs);
    }

    @Test
    void totalAttributes_levelOneWarrior_baseAttrs() {
        var hero = new Warrior("test1");
        var expectedAttrs = new PrimaryAttributes(5, 2, 1);
        var actualAttrs = hero.totalAttributes();
        assertEquals(expectedAttrs, actualAttrs);
    }
}
