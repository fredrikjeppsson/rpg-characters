package rpg;

import org.junit.jupiter.api.Test;
import rpg.exceptions.InvalidWeaponException;
import rpg.heroes.Warrior;
import rpg.items.ItemSlot;
import rpg.items.Weapon;
import rpg.items.WeaponType;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestWeapons {
    @Test
    void equipWeapon_LevelTooHigh_InvalidWeaponException() {
        var hero = new Warrior("level 1");
        var weapon = new Weapon("Level 2 Axe", 2, ItemSlot.WEAPON,
                WeaponType.AXE, 7, 1.1);
        assertThrows(InvalidWeaponException.class, () -> hero.equip(weapon));
    }

    @Test
    void equipWeapon_wrongWeaponType_InvalidWeaponException() {
        var hero = new Warrior("level 1");
        var weapon = new Weapon("Level 1 Bow", 1, ItemSlot.WEAPON,
                WeaponType.BOW, 10, 1);
        assertThrows(InvalidWeaponException.class, () -> hero.equip(weapon));
    }

    @Test
    void equipWeapon_validWeapon_true() {
        var hero = new Warrior("level 1");
        var weapon = new Weapon("Level 1 Sword", 1, ItemSlot.WEAPON,
                WeaponType.SWORD, 10, 3);
        var result = hero.equip(weapon);
        assertTrue(result);
    }
}
