package rpg;

import org.junit.jupiter.api.Test;
import rpg.heroes.PrimaryAttributes;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPrimaryAttributes {
    @Test
    void add_addingToAttributes_attributesAfterAdd() {
         var baseAttrs = new PrimaryAttributes(1,1,1);
         var toAdd = new PrimaryAttributes(99,99,99);
         var result = baseAttrs.add(toAdd);
         var expected = new PrimaryAttributes(100,100,100);
         assertEquals(expected, result);
    }

    @Test
    void multiply_multiplyingAttributes_attributesAfterMultiply() {
        var baseAttrs = new PrimaryAttributes(10, 10, 10);
        var timesNine = baseAttrs.multiply(9);
        var expected = new PrimaryAttributes(90, 90, 90);
        assertEquals(expected, timesNine);
    }
}
