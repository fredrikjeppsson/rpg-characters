package rpg;

import org.junit.jupiter.api.Test;
import rpg.heroes.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests that attributes are correct after level up.
 */
public class TestLeveledAttributes {
    @Test
    void totalAttributes_levelNineMage_LeveledAttrs() {
        var hero = new Mage("level 9");
        hero.levelUp(8);
        var baseAttrs = new PrimaryAttributes(1,1, 8);
        var fromLevels = new PrimaryAttributes(1, 1, 5).multiply(8);
        var expectedAttrs = baseAttrs.add(fromLevels);
        var actualAttrs = hero.totalAttributes();
        assertEquals(expectedAttrs, actualAttrs);
    }

    @Test
    void totalAttributes_levelNineWarrior_LeveledAttrs() {
        var hero = new Warrior("level 9");
        hero.levelUp(8);
        var baseAttrs = new PrimaryAttributes(5,2, 1);
        var fromLevels = new PrimaryAttributes(3, 2, 1).multiply(8);
        var expectedAttrs = baseAttrs.add(fromLevels);
        var actualAttrs = hero.totalAttributes();
        assertEquals(expectedAttrs, actualAttrs);
    }

    @Test
    void totalAttributes_levelNineRanger_LeveledAttrs() {
        var hero = new Ranger("level 9");
        hero.levelUp(8);
        var baseAttrs = new PrimaryAttributes(1,7, 1);
        var fromLevels = new PrimaryAttributes(1, 5, 1).multiply(8);
        var expectedAttrs = baseAttrs.add(fromLevels);
        var actualAttrs = hero.totalAttributes();
        assertEquals(expectedAttrs, actualAttrs);
    }

    @Test
    void totalAttributes_levelNineRogue_LeveledAttrs() {
        var hero = new Rogue("level 9");
        hero.levelUp(8);
        var baseAttrs = new PrimaryAttributes(2,6, 1);
        var fromLevels = new PrimaryAttributes(1, 4, 1).multiply(8);
        var expectedAttrs = baseAttrs.add(fromLevels);
        var actualAttrs = hero.totalAttributes();
        assertEquals(expectedAttrs, actualAttrs);
    }

}
