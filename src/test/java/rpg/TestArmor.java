package rpg;

import org.junit.jupiter.api.Test;
import rpg.exceptions.InvalidArmorException;
import rpg.heroes.PrimaryAttributes;
import rpg.heroes.Warrior;
import rpg.items.Armor;
import rpg.items.ArmorType;
import rpg.items.ItemSlot;

import static org.junit.jupiter.api.Assertions.*;

public class TestArmor {
    @Test
    void equipArmor_LevelTooHigh_InvalidArmorException() {
        var hero = new Warrior("level 1");
        var armor = new Armor("Level 2 Plate", 2, ItemSlot.BODY,
                ArmorType.PLATE, new PrimaryAttributes(1, 1, 1));
        assertThrows(InvalidArmorException.class, () -> hero.equip(armor));
    }

    @Test
    void equipArmor_wrongArmorType_InvalidArmorException() {
        var hero = new Warrior("level 1");
        var armor = new Armor("Level 1 Cloth", 1, ItemSlot.BODY,
                ArmorType.CLOTH, new PrimaryAttributes(1, 1, 1));
        assertThrows(InvalidArmorException.class, () -> hero.equip(armor));
    }

    @Test
    void equipArmor_validEquip_true() {
        var hero = new Warrior("level 1");
        var armor = new Armor("Level 1 Cloth", 1, ItemSlot.BODY,
                ArmorType.PLATE, new PrimaryAttributes(1, 1, 1));
        var result = hero.equip(armor);
        assertTrue(result);
    }

    @Test
    void equipArmor_totalAttributes_increaseStrByOne() {
        var hero = new Warrior("level 1");
        hero.equip(new Armor("+1 to all", 1, ItemSlot.BODY,
                ArmorType.PLATE, new PrimaryAttributes(1, 1, 1)));
        var attrs = hero.totalAttributes();
        var expectedAttrs = new PrimaryAttributes(6, 3, 2);
        assertEquals(expectedAttrs, attrs);
    }
}
