package rpg;

import org.junit.jupiter.api.Test;
import rpg.heroes.PrimaryAttributes;
import rpg.heroes.Ranger;
import rpg.heroes.Warrior;
import rpg.items.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestDamage {
    @Test
    void characterDps_noWeapon_baseDps() {
        var hero = new Warrior("level 1");
        var dps = hero.characterDPS();
        var expectedDps = 1 * (1 + (5 / 100.0));
        assertEquals(expectedDps, dps);
    }

    @Test
    void characterDps_Level1Axe_currentDps() {
        var hero = new Warrior("level 1");
        hero.equip(new Weapon("level 1 Axe", 1, ItemSlot.WEAPON,
                WeaponType.AXE, 7, 1.1));
        var dps = hero.characterDPS();
        var expectedDps = (7 * 1.1)* (1 + (5 / 100.0));
        assertEquals(expectedDps, dps);
    }

    @Test
    void characterDps_bonusFromArmor_currentDps() {
        var hero = new Warrior("level 1");
        hero.equip(new Weapon("level 1 Axe", 1, ItemSlot.WEAPON,
                WeaponType.AXE, 7, 1.1));
        hero.equip(new Armor("level 1 Plate", 1, ItemSlot.BODY,
                ArmorType.PLATE, new PrimaryAttributes(1, 0, 0)));
        var dps = hero.characterDPS();
        var expectedDps =  (7 * 1.1) * (1 + ((5+1) / 100.0));
        assertEquals(expectedDps, dps);
    }

    @Test
    void characterDps_highLevelCharacter_currentDps() {
        var hero = new Ranger("Legolas");
        hero.levelUp(98);
        hero.equip(new Weapon("Bow of the Galadhrim", 99, ItemSlot.WEAPON,
                WeaponType.BOW, 78, 1.8));
        var dps = hero.characterDPS();
        var dexAtLevel99 = 7 + (5 * 98);
        var bowDps = 78 * 1.8;
        var expectedDPS = bowDps * (1 + dexAtLevel99 / 100.0);
        assertEquals(expectedDPS, dps);
    }
}
