package rpg.items;

import rpg.heroes.PrimaryAttributes;

public class Armor extends Item {
    private final ArmorType aType;

    public Armor(String name, int requiredLevel, ItemSlot slot,
                 ArmorType aType, PrimaryAttributes bonusAttributes) {
        super(name, requiredLevel, slot, bonusAttributes);
        this.aType = aType;
    }

    public ArmorType getArmorType() {
        return aType;
    }
}
