package rpg.items;

import rpg.heroes.PrimaryAttributes;

abstract public class Item {
    private final String name;
    private final int requiredLevel;
    private final ItemSlot slot;
    private final PrimaryAttributes bonusAttributes;

    public Item(String name, int requiredLevel, ItemSlot slot, PrimaryAttributes bonusAttributes) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
        this.bonusAttributes = bonusAttributes;
    }

    public PrimaryAttributes getBonusAttributes() {
        return bonusAttributes;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public ItemSlot getSlot() {
        return slot;
    }
}
