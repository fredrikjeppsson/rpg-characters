package rpg.items;

public enum ItemSlot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
