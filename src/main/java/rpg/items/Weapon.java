package rpg.items;

import rpg.heroes.PrimaryAttributes;

public class Weapon extends Item {
    private final WeaponType wType;
    private final double damage;
    private final double attackSpeed;

    public Weapon(String name, int requiredLevel, ItemSlot slot,
                  WeaponType wType, double damage, double attackSpeed) {
        // Weapons don't have bonus attributes in the spec. This is solved by adding (0, 0, 0)
        // as a weapon's bonus attributes. This simplifies the code and makes it easier to add
        // bonuses to weapons in the future.
        super(name, requiredLevel, slot, new PrimaryAttributes(0,0,0));
        this.wType = wType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    public double weaponDPS() {
        return damage * attackSpeed;
    }

    public WeaponType getWeaponType() {
        return wType;
    }
}

