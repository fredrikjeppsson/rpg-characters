package rpg.items;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
