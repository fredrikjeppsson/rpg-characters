package rpg.heroes;

import rpg.exceptions.InvalidArmorException;
import rpg.exceptions.InvalidWeaponException;
import rpg.items.*;

import java.util.HashMap;
import java.util.Set;
import java.util.StringJoiner;

abstract public class Hero {
    private final String name;
    private int level;
    private final PrimaryAttributes baseAttributes;
    private final PrimaryAttributes increasePerLevel;
    private final HashMap<ItemSlot, Item> equipment;
    private final Set<ArmorType> validArmorTypes;
    private final Set<WeaponType> validWeaponTypes;

    abstract int primaryAttribute();

    public Hero(String name,
                int level,
                PrimaryAttributes baseAttributes,
                PrimaryAttributes increasePerLevel,
                HashMap<ItemSlot, Item> equipment,
                Set<ArmorType> validArmorTypes,
                Set<WeaponType> validWeaponTypes) {
        this.name = name;
        this.level = level;
        this.baseAttributes = baseAttributes;
        this.increasePerLevel = increasePerLevel;
        this.equipment = equipment;
        this.validArmorTypes = validArmorTypes;
        this.validWeaponTypes = validWeaponTypes;

        // A new hero is equipped with 'unequipped' to avoid having to deal with the special
        // case of a hero that has never equipped a weapon.
        equip(new Weapon("unequipped", 1, ItemSlot.WEAPON,
                WeaponType.UNEQUIPPED, 1, 1));
    }

    public PrimaryAttributes totalAttributes() {
        var levelBonus = increasePerLevel.multiply(level - 1);
        var charAttrs = baseAttributes.add(levelBonus);

        for (var entry : equipment.entrySet()) {
            var item = entry.getValue();
            charAttrs = charAttrs.add(item.getBonusAttributes());
        }

        return charAttrs;
    }

    public void levelUp() {
        level += 1;
    }

    public void levelUp(int nbrOfLevels) {
        level += nbrOfLevels;
    }

    public boolean equip(Weapon weapon) {
        if (!validWeaponTypes.contains(weapon.getWeaponType())) {
            throw new InvalidWeaponException("Weapon of type " + weapon.getWeaponType() + " cannot be equipped.");
        }

        if (weapon.getRequiredLevel() > level) {
            throw new InvalidWeaponException("Weapon requires level " + weapon.getRequiredLevel() + " but hero is level " + level);
        }

        equipment.put(ItemSlot.WEAPON, weapon);
        return true;
    }

    public boolean equip(Armor armor) {
        if (!validArmorTypes.contains(armor.getArmorType())) {
            throw new InvalidArmorException("Armor of type " + armor.getArmorType() + " cannot be equipped.");
        }

        if (armor.getRequiredLevel() > level) {
            throw new InvalidArmorException("Armor requires level " + armor.getRequiredLevel() + " but hero is level " + level);
        }

        equipment.put(armor.getSlot(), armor);
        return true;
    }

    public Weapon getWeapon() {
        return (Weapon) equipment.get(ItemSlot.WEAPON);
    }

    public double characterDPS() {
        return getWeapon().weaponDPS() * (1 + primaryAttribute() / 100.0);
    }

    public String characterSheet() {
        var attrs = totalAttributes();
        var stats = new StringJoiner("\n");
        stats.add("Name: " + name);
        stats.add("Level: " + level);
        stats.add("Strength: " + attrs.strength());
        stats.add("Dexterity: " + attrs.dexterity());
        stats.add("Intelligence: " + attrs.intelligence());
        stats.add("DPS: " + characterDPS());
        return stats.toString();
    }

    public static void main(String[] args) {
        // manual test of character sheet.
        var hero = new Ranger("Legolas");
        hero.levelUp(98);
        hero.equip(new Weapon("Bow of the Galadhrim", 99, ItemSlot.WEAPON,
                WeaponType.BOW, 78, 1.8));
        System.out.println(hero.characterSheet());
    }
}
