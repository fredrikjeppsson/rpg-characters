package rpg.heroes;

import rpg.items.ArmorType;
import rpg.items.WeaponType;

import java.util.HashMap;
import java.util.Set;

public class Rogue extends Hero {
    public Rogue(String name) {
        super(
                name,
                1,
                new PrimaryAttributes(2, 6, 1),
                new PrimaryAttributes(1, 4, 1),
                new HashMap<>(),
                Set.of(ArmorType.LEATHER, ArmorType.MAIL),
                Set.of(WeaponType.DAGGER, WeaponType.SWORD, WeaponType.UNEQUIPPED)
        );
    }

    @Override
    int primaryAttribute() {
        return totalAttributes().dexterity();
    }
}
