package rpg.heroes;

import rpg.items.ArmorType;
import rpg.items.WeaponType;

import java.util.HashMap;
import java.util.Set;

public class Mage extends Hero {
    public Mage(String name) {
        super(
                name,
                1,
                new PrimaryAttributes(1, 1, 8),
                new PrimaryAttributes(1,1, 5),
                new HashMap<>(),
                Set.of(ArmorType.CLOTH),
                Set.of(WeaponType.STAFF, WeaponType.WAND, WeaponType.UNEQUIPPED)
        );
    }

    @Override
    int primaryAttribute() {
        return totalAttributes().intelligence();
    }
}
