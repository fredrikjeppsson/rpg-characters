package rpg.heroes;

import rpg.items.ArmorType;
import rpg.items.WeaponType;

import java.util.HashMap;
import java.util.Set;

public class Warrior extends Hero {
    public Warrior(String name) {
        super(
                name,
                1,
                new PrimaryAttributes(5, 2, 1),
                new PrimaryAttributes(3, 2, 1),
                new HashMap<>(),
                Set.of(ArmorType.MAIL, ArmorType.PLATE),
                Set.of(WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD, WeaponType.UNEQUIPPED)
        );
    }

    @Override
    int primaryAttribute() {
        return totalAttributes().strength();
    }}
