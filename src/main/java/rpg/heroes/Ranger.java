package rpg.heroes;

import rpg.items.ArmorType;
import rpg.items.WeaponType;

import java.util.HashMap;
import java.util.Set;

public class Ranger extends Hero {
    public Ranger(String name) {
        super(
                name,
                1,
                new PrimaryAttributes(1, 7, 1),
                new PrimaryAttributes(1,5, 1),
                new HashMap<>(),
                Set.of(ArmorType.LEATHER, ArmorType.MAIL),
                Set.of(WeaponType.BOW, WeaponType.UNEQUIPPED)
        );
    }

    @Override
    int primaryAttribute() {
        return totalAttributes().dexterity();
    }
}
