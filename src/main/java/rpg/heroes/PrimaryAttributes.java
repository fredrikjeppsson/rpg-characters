package rpg.heroes;

public record PrimaryAttributes(int strength, int dexterity, int intelligence) {

    public PrimaryAttributes multiply(int factor) {
        return new PrimaryAttributes(
                strength * factor,
                dexterity * factor,
                intelligence * factor
        );
    }

    public PrimaryAttributes add(PrimaryAttributes other) {
        return new PrimaryAttributes(
                strength + other.strength,
                dexterity + other.dexterity,
                intelligence + other.intelligence
        );
    }

    @Override
    public String toString() {
        return "PrimaryAttributes{" +
                "strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                '}';
    }
}
