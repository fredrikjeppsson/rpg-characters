package rpg.exceptions;

public class InvalidWeaponException extends InvalidItemException {
    public InvalidWeaponException() {
    }

    public InvalidWeaponException(String message) {
        super(message);
    }

    public InvalidWeaponException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidWeaponException(Throwable cause) {
        super(cause);
    }

    public InvalidWeaponException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
