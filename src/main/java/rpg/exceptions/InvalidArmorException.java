package rpg.exceptions;

public class InvalidArmorException extends InvalidItemException {
    public InvalidArmorException() {
    }

    public InvalidArmorException(String message) {
        super(message);
    }

    public InvalidArmorException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidArmorException(Throwable cause) {
        super(cause);
    }

    public InvalidArmorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
